function createCard(name, description, pictureUrl, startDate, endDate, locationName) {
  const formattedStartDate = new Date(startDate).toLocaleDateString('en-US', { year: 'numeric', month: 'long', day: 'numeric' });
  const formattedEndDate = new Date(endDate).toLocaleDateString('en-US', { year: 'numeric', month: 'long', day: 'numeric' });

  return `
    <div class="col mb-4">
      <div class="card shadow">
        <img src="${pictureUrl}" class="card-img-top" alt="${name}">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${locationName}</h6>
          <p class="card-text">${description}</p>
        </div>
        <div class="card-footer text-muted">
          ${formattedStartDate} - ${formattedEndDate}
        </div>
      </div>
    </div>
  `;
}

  window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/';
    const row = document.querySelector('.row'); // Ensure your HTML has a div with class 'row'

    try {
      const response = await fetch(url);
      if (!response.ok) {
        throw new Error(`HTTP error! Status: ${response.status}`);
      }

      const data = await response.json();
      for (let conference of data.conferences) {
        const detailUrl = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailUrl);
        if (!detailResponse.ok) {
          throw new Error(`Status: ${detailResponse.status}`);
        }
        const details = await detailResponse.json();
        const html = createCard(
          conference.name,
          details.conference.description,
          details.conference.location.picture_url,
          details.conference.starts,
          details.conference.ends,
          details.conference.location.name
        );
        row.innerHTML += html; // Append the new card to the row
      }
    } catch (e) {
      console.error('Error fetching data:', e);
    }
  });