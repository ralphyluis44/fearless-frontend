import React from 'react';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-light bg-light">
      <a className="navbar-brand" href="/">Conference GO!</a>
      <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
        <span className="navbar-toggler-icon"></span>
      </button>
      <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
        <div className="navbar-nav">
          <a className="nav-item nav-link active" href="/">Home </a>
          <a className="nav-item nav-link" href="/location">New Location</a>
          <a className="nav-item nav-link" href="/conference">New Conference</a>
          <a className="nav-item nav-link" href="/presentation">New Presentation</a>
        </div>
      </div>
    </nav>
  );
}

export default Nav;
